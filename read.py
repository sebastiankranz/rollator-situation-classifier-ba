from typing import List, Dict, Any

import pandas as pd

import schemas


class SampleSource:
    def __init__(self, path: str, label: str):
        self.path = path
        self.label = label


def adjust_time(data: pd.DataFrame):
    # print(data)
    start_time = data.at[0, "time"]

    def adjust_column(x):
        return (x - start_time) * 10 ** (-9)

    return data.assign(time=adjust_column(data["time"]))


def read_source(source: SampleSource) -> pd.DataFrame:
    frame = pd.read_csv("./data/" + source.path,
                        index_col=False,
                        skip_blank_lines=True,
                        comment="%",
                        sep=",")
    return adjust_time(frame)


def read_file(path: str, should_adjust_time=True):
    frame = pd.read_csv("./data/" + path,
                        index_col=False,
                        skip_blank_lines=True,
                        comment="%",
                        sep=",")
    if should_adjust_time:
        return adjust_time(frame)
    else:
        return frame
