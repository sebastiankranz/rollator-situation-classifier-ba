# %% Setup
from typing import Dict, Iterable, SupportsFloat

import numpy as np
import pandas as pd
import pywt


def wavelet_transform(data: pd.Series, wavelet):
    result = pywt.wavedec(data, wavelet=wavelet)
    final_aprox_coeff = result[0]
    low_passes = result[1:]
    return low_passes, final_aprox_coeff


def get_statistics(values: Iterable) -> Dict[str, SupportsFloat]:
    def crossing_rate(values, line=0):
        adjusted_values = values - line
        return ((adjusted_values[: -1] * adjusted_values[1:]) < 0).sum()

    mean = np.mean(values)
    return {
        "Mean": mean,
        "Variance": np.var(values),
        "Mean Crossing Rate": crossing_rate(values, mean),
        "n25": np.quantile(values, .2),
        "n50": np.quantile(values, .5),
        "n75": np.quantile(values, .7),
        "max": np.max(values),
        "min": np.min(values)
    }






