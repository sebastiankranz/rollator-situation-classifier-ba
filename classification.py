from multiprocessing.pool import Pool
from typing import List, Callable, Tuple

import pandas as pd
from scipy.stats import uniform
from sklearn import svm
from toolz import curry, pipe
from sklearn.model_selection import KFold, train_test_split, GridSearchCV


def drop_labels(data: pd.DataFrame) -> pd.DataFrame:
    return data.drop("label", axis="columns")


def split_by_ratio(ratio, data: pd.DataFrame):
    split_point = int(len(data) * ratio)
    return (data.iloc[:split_point],
            data.iloc[split_point:])


@curry
def split_by_indices(train_indices, test_indices, data: pd.DataFrame):
    return (data.iloc[train_indices], data.iloc[test_indices])


def do_kfold_cross_validation(splits: int,
                              get_score: Callable[[pd.DataFrame, pd.DataFrame], int],
                              data: pd.DataFrame) -> List[int]:
    kfold = KFold(n_splits=splits)
    with Pool(12) as p:
        return p.starmap(get_score,
                         [(data.iloc[train], data.iloc[test]) for train, test in kfold.split(data)])


def score_with_probability(args,
                           training_data, training_labels,
                           testing_data, testing_labels):
    svc = svm.SVC(**args, probability=True)
    svc.fit(training_data, training_labels)
    score = svc.score(testing_data, testing_labels)
    predictions = pipe(svc.predict_proba(testing_data),
                       lambda it: pd.DataFrame(it, columns=svc.classes_),
                       lambda it: it.assign(label=testing_labels.reset_index(drop=True)))
    return score, predictions


def predict(args,
            training_data, training_labels,
            testing_data, testing_labels):
    svc = svm.SVC(**args)
    svc.fit(training_data, training_labels)
    score = svc.score(testing_data, testing_labels)
    predictions = svc.predict(testing_data)
    return score, predictions


# def hyperparameter_search():
#     svc = svm.SVC()
#
#     param_grid = {"C": [2 ^ i for i in range(-5, 15)],
#                   "Gamma": [2 ^ i for i in range(-15, 3)]}
#
#     search = GridSearchCV(svc,
#                           param_grid,
#                           cv=5,
#                           n_jobs=-1)
#
#     search.fit(, y)
