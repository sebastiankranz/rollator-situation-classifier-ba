from typing import List, Any, Tuple

import cupy as cp
import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import log_loss, accuracy_score, balanced_accuracy_score
from sklearn.preprocessing import LabelBinarizer
from sklearn.utils.class_weight import compute_class_weight, compute_sample_weight
from pathos.multiprocessing import ProcessingPool as Pool

from util import (
    MultivariateTimeSeries,
    generate_centroids,
    log_time, log_duration, print_warning)

## uncomment, if Cuda is unavailable
# cp = np
np.seterr("raise", over="warn", under="ignore")


def check_nums(a, name, nan=True, inf=True, zero=True):
    if nan and cp.isnan(a).any():
        raise Exception(f"{name} has nan")
    if inf and cp.isinf(a).any():
        print_warning(f"{name} has inf")
    if zero and cp.count_nonzero(a) < (a.size / 2):
        print_warning(f"{name} has a lot of zeroes")
        print(a)


def replace_inf(a):
    return np.nan_to_num(a, nan=cp.nan, neginf=0, posinf=0)
    # neginf=np.finfo(np.float64).min,
    # posinf=np.finfo(np.float64).max)


class LTSClassifier(BaseEstimator, ClassifierMixin):

    def __init__(self,
                 min_iter: int = 1,
                 max_iter: int = 1,
                 L: List[int] = [60, 40, 20],
                 K: int = 1,
                 alpha: float = 100,
                 eta: float = 0.01,
                 class_weight=None,
                 lambdaW: int = 0.01,
                 post_init_callback=None,
                 iter_callback=None,
                 abort_condidtion=None):
        self.min_iter = min_iter
        self.max_iter = max_iter
        self.L = np.array(L)
        self.K = K
        self.alpha = alpha
        self.eta = eta
        self.class_weight = class_weight
        self.lambdaW = lambdaW
        self.iter_callback = iter_callback
        self.post_init_callback = post_init_callback
        self.abort_condition = abort_condidtion

    def get_segments(self, x):
        L = self.L
        L_R = max(L)
        I, Q, M = x.shape
        R = len(L)
        J = Q - L  # r
        J_R = max(J)

        T = cp.zeros((I, R, J_R, L_R, M))
        for i in range(I):
            for r in range(R):
                for j in range(J[r]):
                    T[i, r, j, :L[r], :] = cp.array(x[i, j:(j + L[r]), :])
        return T

    def free_memory(self):
        with log_duration("shelving training segments"):
            self.shelved_T = cp.asnumpy(self.T)
            self.T = None

    def load_to_memory(self):
        with log_duration("loading training segments"):
            self.T = cp.array(self.shelved_T)

    def fit(self, x, g):
        self.binarizer = LabelBinarizer().fit(g)

        I, Q, M = x.shape
        C = len(self.binarizer.classes_)
        K = self.K
        L = self.L  # r
        L_R = max(self.L)
        R = len(L)
        self.Lambda = compute_class_weight(self.class_weight, np.unique(g), g)
        Lambda = cp.array(self.Lambda)  # c
        J = Q - L  # r
        J_R = max(J)

        B = 12
        batch_count = int(np.ceil(I / B))

        self.T = self.get_segments(x)

        self.S = cp.zeros((R, K, L_R, M))  # cp.full((R, K, L_R, M), 0)
        for r in range(R):
            centroids = generate_centroids(x, L[r], K)
            for k in range(K):
                self.S[r, k, :L[r], :] = cp.array(centroids[k, :, :])
        self.W = cp.zeros((C, R, K))
        self.W0 = cp.zeros((C))

        Y = cp.array(self.binarizer.transform(g))  # i, c
        print(Y)

        # Variables that can be computed beforehand to save time during the iterations
        twolambda_IC = (2 * self.lambdaW) / (I * C)
        ML = cp.array(M * L)  # r

        if self.post_init_callback is not None:
            self.post_init_callback(self)

        for iter in range(self.max_iter):

            def ch_n(a, name):
                check_nums(a[0, :, :J[0]], name)
                check_nums(a[1, :, :J[1]], name)
                check_nums(a[2, :, :J[2]], name)

            for i in range(I):
                # order of dimensions: iter i c r k j l m

                # r k j m l
                distances = self.T[i, :, None, :, :, :] - self.S[:, :, None, :, :]

                D = (cp.sum(distances ** 2, axis=(3, 4)) / ML[:, None, None])  # r k j
                epsilon = cp.e ** (-self.alpha * D)  # r k j

                psi = cp.sum(epsilon, axis=2)  # r k
                D_min = cp.sum(D * epsilon, axis=2) / psi  # r k
                D_min[cp.isfinite(D_min) == False] = 10 #np.finfo(np.float64).max

                if (D_min[D_min > 10].any()):
                    print_warning(str(D_min.max))
                #check_nums(D_min, "D_min")
                # print(cp.max(D_min))
                # print(cp.min(D_min))

                P = (1 + cp.e ** (-self.W0 - cp.sum(D_min[None, :, :] * self.W[:, :, :],
                                                    axis=(1, 2)))) ** -1  # i c
                theta = Y[i, :] - P  # c
                #check_nums(theta, "theta", zero=False)

                self.W += self.Lambda * self.eta * (
                    (theta[:, None, None] * D_min[None, :, :]) - (twolambda_IC * self.W[:, :, :])
                )  # c r k

                phi = (2 * epsilon * (1 - (self.alpha * (D[:, :, :] - D_min[:, :, None])))) / (
                        ML[:, None, None] * psi[:, :, None])  # r k j
                phi[cp.isfinite(phi) == False] = 0
                #check_nums(phi, "phi")

                self.S += cp.sum(
                    self.Lambda
                    * self.eta
                    * theta[:, None, None, None, None, None]  # c
                    * phi[None, :, :, :, None, None]  # r k j
                    * -distances[None, :, :, :, :, :]  # r k j m l
                    * self.W[:, :, :, None, None, None]  # c r k
                , axis = (0, 3))  # sum: c j  ->  r, k, m, l

                self.W0 += self.Lambda * self.eta * theta

            check_nums(self.W0, "W0")
            check_nums(self.W, "W")
            check_nums(self.S[0, :, :L[0], :], "S")
            check_nums(self.S[1, :, :L[1], :], "S")
            check_nums(self.S[2, :, :L[2], :], "S")

            if self.iter_callback is not None:
                self.iter_callback(self, iter)

            if self.abort_condition is not None and self.abort_condition(self):
                return


    def score(self, x: List[MultivariateTimeSeries], y: List[str], **kwargs):
        y_hat, _ = self.predict(x)
        return accuracy_score(y, y_hat, sample_weight=compute_sample_weight("balanced", y))


    def predict(self, x):
        I, Q, M = x.shape
        L = self.L
        R = len(L)
        K = self.K
        J = Q - L

        ML = M * cp.array(L)

        x = cp.array(x)
        T = self.get_segments(x)

        def model(i):
            distances = (T[i, :, None, :, :, :] - self.S[:, :, None, :, :])  # r k j l m
            D = cp.sum(distances ** 2, axis=(3, 4)) / ML[:, None, None]  # r k j

            epsilon = cp.e ** (-self.alpha * D)  # r k j

            psi = cp.sum(epsilon, axis=2)  # r k

            D_min = cp.sum(D * epsilon, axis=2) / psi  # r k
            D_min[cp.isfinite(D_min) == False] = 10 #np.finfo(np.float64).max
            check_nums(D_min, "D_min")

            return cp.asnumpy(1 / (1 + (cp.e ** (- self.W0 - cp.sum(D_min[None, :, :]
                                                                    * self.W,
                                                                    axis=(1, 2))))))  # c

        P = np.array([model(i) for i in range(I)])  # i c

        return self.binarizer.inverse_transform(P, threshold=np.max(P)), P


    def state_summary(self, test_x, test_y, train_x, train_y):
        test_predictions, test_probabilities = self.predict(test_x)
        train_predictions, train_probabilities = self.predict(train_x)

        def score(predictions, probabilities, y):
            sample_weight = compute_sample_weight("balanced", y)
            accuracy = balanced_accuracy_score(y, predictions)

            loss = log_loss(y, replace_inf(probabilities), sample_weight=sample_weight)
            return accuracy, loss

        train_accuracy, train_loss = score(train_predictions, train_probabilities, train_y)
        test_accuracy, test_loss = score(test_predictions, test_probabilities, test_y)

        R = len(self.S)

        return {
            "S": [cp.asnumpy(self.S[r]) for r in range(R)],
            "W": [cp.asnumpy(self.W[r]) for r in range(R)],
            "W_0": [cp.asnumpy(self.W0[r]) for r in range(R)],
            "train_acc": train_accuracy,
            "train_loss": train_loss,
            "train_probabilities": train_probabilities,
            "train_predictions": train_predictions,
            "test_acc": test_accuracy,
            "test_loss": test_loss,
            "test_probabilities": test_probabilities,
            "test_predictions": test_predictions
        }
