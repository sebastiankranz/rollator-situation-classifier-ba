import shelve
from contextlib import contextmanager
from typing import Any

def get_location(name = "saved_session"):
    location = f"./results/{name}.out"

def save_session(globals=None):
    if globals is None:
        globals = globals
    with open_shelve(get_location()) as my_shelf:
        for key, value in globals.items():
            if not key.startswith('__'):
                try:
                    my_shelf[key] = value
                except Exception:
                    print('ERROR shelving: "%s"' % key)
                else:
                    print('shelved: "%s"' % key)

def load_session():
    with open_shelve(get_location()) as my_shelf:
        for key in my_shelf:
            try:
                globals()[key]=my_shelf[key]
            except Exception:
                print('ERROR restoring: "%s"' % key)
            else:
                print('restored: "%s"' % key)


@contextmanager
def open_shelve(location):
    s = shelve.open(location)
    yield s
    s.close()



