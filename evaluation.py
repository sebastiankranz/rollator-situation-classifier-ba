from dataclasses import dataclass

import numpy as np
from sklearn.metrics import accuracy_score, log_loss
from sklearn.utils import compute_sample_weight
import matplotlib.pyplot as plt


def predict_remining_time(time_passed, progress, total):
    time_per_unit = time_passed / progress
    remaining = time_per_unit * total
    return (remaining, time_per_unit)



def init_score_plot():
    fig, axs = plt.subplots(nrows=1, ncols=2)
    fig.show()
    fig.canvas.draw()
    plt.pause(1)

    def update(max_iter, df):
        for ax, (acc, loss) in zip(axs, [("train_acc", "test_acc"), ("train_loss", "test_loss")]):
            ax.clear()
            df.plot(y=acc, x="iter", ax=ax)
            df.plot(y=loss, x="iter", ax=ax)
            #ax.set_xlim(0, max_iter)
            ax.legend()
            #ax.set_xlim(0, max_iter)
        fig.canvas.draw()

    return update


def init_shapelet_plot(R, K):
    fig, axs = plt.subplots(nrows=R, ncols=K, figsize=(20, 8))
    fig.show()
    fig.canvas.draw()
    plt.pause(1)

    def update(S, W):
        for r in range(0, R):
            _, _, M = S[r].shape
            for k in range(0, K):
                axs[r, k].clear()
                for m in range(0, M):
                    axs[r, k].plot(S[r][k, :, m])
        fig.canvas.draw()

    return update
