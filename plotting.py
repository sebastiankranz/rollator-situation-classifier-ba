# %% Setup
# %matplotlib notebook
from contextlib import contextmanager
from typing import Union, List

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
from toolz import pipe
from toolz.curried import filter

import classification as cl
import read as r
import schemas
import util
import model as md
from toolz.curried import map

matplotlib.rc("axes.formatter", useoffset=False)


def show_plot(
    data: pd.DataFrame, columns: Union[List[str], str], title: str = ""
):
    cols = pipe(columns, filter(lambda it: it in data.columns), list)

    data.plot(x="time", y=cols)
    plt.title(title)

def create_live_plot(draw_function):
    fig = plt.figure()
    fig.show()
    fig.canvas.draw()
    plt.pause(1)

    def update(**kwargs):
        fig.clf()
        draw_function(fig, **kwargs)
        fig.canvas.draw()
        plt.pause(1)

    return update



def plot_sensordata(data: pd.DataFrame):
    show_plot(data, columns=md.columns[1:7], title="IR")
    show_plot(data, columns=md.columns[7:9] + md.columns[22:24], title="Force and Odo")
    show_plot(data, columns=md.columns[9:13], title="Quat")
    show_plot(data, columns=md.columns[13:16], title="Angular Velocity")
    show_plot(data, columns=md.columns[16:19], title="Linear Acceleration")
    show_plot(data, columns=md.columns[19:22], title="Mag")

def plot_confusion_matrix(fig, predictions, labels, normalize=True):
    classes = pipe(unique_labels(labels), map(str))
    matrix = confusion_matrix(labels, predictions)
    cm = (
        matrix.astype("float") / matrix.sum(axis=1)[:, np.newaxis]
        if normalize
        else matrix
    )

    ax = fig.subplots()
    im = ax.imshow(cm, interpolation="nearest")
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(
        xticks=np.arange(cm.shape[1]),
        yticks=np.arange(cm.shape[0]),
        # ... and label them with the respective list entries
        xticklabels=classes,
        yticklabels=classes,
        ylabel="True label",
        xlabel="Predicted label",
    )

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = ".2f" if normalize else "d"
    thresh = cm.max() / 2.0
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(
                j,
                i,
                format(cm[i, j], fmt),
                ha="center",
                va="center",
                color="black" if cm[i, j] > thresh else "white",
            )
    fig.tight_layout()
    return ax


@contextmanager
def make_figure(show=True):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    yield ax
    if show:
        plt.show()


def plot_probability_predictions(predictions: pd.DataFrame, onclick):

    with make_figure(show=False) as ax:
        ax.pcolor(predictions.select_dtypes(include=np.number))
        ax.set_xticklabels(predictions.columns.values)
        plt.connect("button_release_event", onclick)



file_names = [
    # "2019_1_15_18_4_Outdoor.txt", "Sebastian/standing.txt",
    # "Sebastian/idle.txt",
    "Sebastian/walking.csv",
    # "Sebastian/2019_1_30_13_45.txt",
    # "Sebastian/step_up.txt",
    # "Sebastian/sit_down.txt", "Sebastian/sit_up.txt",
    # "Other/2019_4_11_15_0_sitzen_nr2.txt", "Other/2019_4_11_15_25_sitzen_nr3.txt"
    # "Sebastian/sit_up.txt"
    # "Sebastian/step_up.txt"
]

if __name__ == "__main__":
    for path in file_names:
        data = r.read_file(path, should_adjust_time=False)
        print(data)
        #standardized_data["marked"] = standardized_data["marked"].astype(int)

        show_plot(
            data,
            ["ir0", "ir1", "ir2", "ir3", "ir4", "fr1", "fr2", "of-interest"],
            title=path,
        )

    plt.show()
