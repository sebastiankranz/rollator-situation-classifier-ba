from functools import reduce
from typing import List, Tuple

import pandas as pd
from pandas.tests.extension.numpy_.test_numpy_nested import np
import matplotlib.pyplot as plt
import pywt
from toolz import pipe

import feature_extraction as fe
from preprocessing import apply_window
from util import enumerate_to_dict, stack_dict, save_csv


def do_dwt(series, wavelet_name) -> List[Tuple[List, List]]:
    wavelet = pywt.Wavelet(wavelet_name)
    cA, cD = pywt.dwt(series, wavelet)

    remaining_levels_count = pywt.dwt_max_level(data_len=len(series), filter_len=wavelet.dec_len)

    return ([(cA, cD), *do_dwt(cA, wavelet_name)]
            if remaining_levels_count > 0
            else [(cA, cD)])


wavelet_name = "sym5"

def test():
    x = np.linspace(0, 1, num=2048)
    chirp = np.sin(250 * np.pi * x ** 2) + 0
    # chirp = apply_window(chirp)

    result = do_dwt(chirp, wavelet_name)

    for row, (cA, cD) in enumerate(result):
        position = (2 * row) + 1
        plt.subplot(len(result), 2, position)
        plt.plot(cA)
        plt.subplot(len(result), 2, position + 1)
        plt.plot(cD)

# plt.show()

# for level, (cA, cD) in enumerate(result):
#     def save(series, title):
#         coordinates = [[x, y] for x, y in enumerate(series)]
#         save_csv("dwt/level" + str(level) + "-" + title,
#                  series,
#                  "x,y")

#     save(cA, "cA")
#     save(cD, "cD")


# stacked = pipe([{"cA": cA, "cD": cD} for cA, cD in result],
#           enumerate_to_dict("level"),
#           stack_dict)
# print(pd.DataFrame(stacked))
