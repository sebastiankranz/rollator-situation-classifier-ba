from pandas.tests.extension.numpy_.test_numpy_nested import np
import matplotlib.pyplot as plt
import pywt
from scipy import signal
from scipy.signal import ricker

import feature_extraction as fe
import plotting as pl

wavelet_max_width = 200
domain_max = 7
nsamples = 4096
sample_density = nsamples / domain_max

x = np.linspace(0, domain_max, num=nsamples)
chirp = np.sin(x ** 3)
# chirp = fe.apply_window(chirp)

with pl.make_figure() as ax:
    ax.plot(chirp)
    wavelet = ricker(200, 5)
    # ax.plot(wavelet)

result = signal.cwt(chirp, signal.ricker, np.arange(1, wavelet_max_width, wavelet_max_width / 100))
result_coordinates = np.array(
    [[x / sample_density, y / (sample_density / 2), z] for (y, x), z in np.ndenumerate(result)])
chirp_coordinates = np.array([[x / sample_density, y] for x, y in enumerate(chirp)])
ricker_coordinates = np.array([[x, y] for x, y in enumerate(wavelet)])

# for i, it in enumerate([*result, chirp]):
#     plt.subplot(len(result)+1, 1, i + 1)
#     plt.plot(it)
#     #plt.ylim(-3, 3)
#     #plt.ylim(lower_limit, upper_limit)

with pl.make_figure() as ax:
    ax.pcolor(result, cmap='BrBG')

np.savetxt("cwt-chirp.csv", result_coordinates, delimiter=",", header="b,a,coeff", comments="")
np.savetxt("chirp.csv", chirp_coordinates, delimiter=",", header="x,y", comments="")
# np.savetxt("ricker.csv", ricker_coordinates, delimiter=",", header="x,y")


plt.show()
