FROM python:3.7.3-stretch

WORKDIR /usr/src/rollator-situation-classifier

COPY ./src .

COPY ./requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt