# %%
import datetime
import multiprocessing
from time import perf_counter
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import cupy as cp
import pandas as pd
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from toolz.curried import mapcat

from classification import drop_labels, split_by_ratio
from evaluation import init_score_plot, init_shapelet_plot
from learning_shapelets import LTSClassifier
from plotting import make_figure, plot_confusion_matrix, create_live_plot
from preprocessing import split_by_time, generate_windows, generate_standardization_func
from read import SampleSource, read_source
from save_session import open_shelve
from util import log_time, generate_centroids, LabeledMultivariateTimeSeries, log_estimated_time
import matplotlib as mpl

mpl.use("TkAgg")

sources: List[SampleSource] = [
    SampleSource("Sebastian/idle.csv", "idle"),
    SampleSource("Sebastian/sitting.csv", "sitting"),
    SampleSource("Sebastian/sit_down.csv", "sit_down"),
    SampleSource("Other/2019_4_11_14_55_sitzen_nr1_runter.csv", "sit_down"),
    SampleSource("Other/2019_4_11_15_0_sitzen_nr2_runter.csv", "sit_down"),
    SampleSource("Other/2019_4_11_15_25_sitzen_nr3_runter.csv", "sit_down"),
    SampleSource("Sebastian/sit_up.csv", "sit_up"),
    SampleSource("Other/2019_4_11_14_55_sitzen_nr1_hoch.csv", "sit_up"),
    SampleSource("Other/2019_4_11_15_0_sitzen_nr2_hoch.csv", "sit_up"),
    SampleSource("Other/2019_4_11_15_25_sitzen_nr3_hoch.csv", "sit_up"),
    SampleSource("2019_1_15_18_4_Outdoor.csv", "walking"),
    SampleSource("Sebastian/walking.csv", "walking"),
    SampleSource("Sebastian/standing.csv", "standing"),
]

for_cl = ["ir1", "ir3", "ir4", "ir5", "ir6", "fr1", "fr2"]
# "linAccX", "linAccY", "linAccZ",
# "angVelZ"]  (Corrupted)

time = datetime.date.today().strftime("%Hh_%Mm_%f_%m_%Y")

max_samples_per_file = None  # 10000

# Split file if time difference between last 2 samples is bigger
max_sample_interval = 0.04

window_size = 128

window_spacing = 32

test_train_ratio = 1 / 3


def get_examples_from_file(source: SampleSource) -> pd.DataFrame:
    samples = read_source(source).head(max_samples_per_file)

    splitted_samples = split_by_time(max_sample_interval, samples)
    windows = mapcat(generate_windows(window_size, window_spacing), splitted_samples)
    windows2 = [window[for_cl].reset_index(drop=True).stack() for window in windows]

    log_time("Created Feature Frame: " + source.label)
    frame = pd.DataFrame(windows2).assign(label=source.label)
    return frame


def get_score(training_data, testing_data):
    svc = svm.SVC(gamma="scale", probability=True)
    svc.fit(drop_labels(training_data), training_data["label"])
    print(repr)
    return svc.score(drop_labels(testing_data), testing_data["label"])


if __name__ == "__main__":
    log_time("Reading Files")
    data_per_situation = [get_examples_from_file(s) for s in sources]
    testing_data_per_situation, training_data_per_situation = zip(
        *[split_by_ratio(test_train_ratio, data)
          for data in data_per_situation
          if not data.empty])

    training_windows = pd.concat(training_data_per_situation).reset_index(drop=True)
    testing_windows = pd.concat(testing_data_per_situation).reset_index(drop=True)

    for sensor in for_cl:
        subframe = training_windows.xs(sensor, level=1, axis=1)
        std_fun = generate_standardization_func(subframe)
        a = std_fun(training_windows.xs(sensor, level=1, drop_level=False, axis=1))
        training_windows.update(
            std_fun(training_windows.xs(sensor, level=1, drop_level=False, axis=1))
        )
        testing_windows.update(
            std_fun(testing_windows.xs(sensor, level=1, drop_level=False, axis=1))
        )

    print(testing_windows)
    print(training_windows)

    training_examples = [
        LabeledMultivariateTimeSeries(
            instance.drop("label").unstack().to_numpy(dtype=np.double),
            instance["label"].iloc[0],
        )
        for i, instance in training_windows.iterrows()
    ]
    testing_examples = [
        LabeledMultivariateTimeSeries(
            instance.drop("label").unstack().to_numpy(dtype=np.double),
            instance["label"].iloc[0],
        )
        for i, instance in testing_windows.iterrows()
    ]
    testing_labels = testing_windows["label"].to_numpy()

    K = 20
    L = [20, 40, 60]
    max_iter = 3000
    default_hyperparams = {
        "max_iter": max_iter,
        "K": K,
        "alpha": 75,
        "eta": 0.01,
        "L": L,
        "lambdaW": 0.01,
        "class_weight": "balanced"
    }

    update_score_plot = init_score_plot()
    update_shapelet_plot = init_shapelet_plot(len(L), K)
    update_initial_shapelet_plot = init_shapelet_plot(len(L), K)
    update_confusion_matrix = create_live_plot(plot_confusion_matrix)
    update_train_confusion_matrix = create_live_plot(plot_confusion_matrix)

    history = pd.DataFrame()

    q = multiprocessing.Queue()

    def evaluate_history(c, iter):
        global history

        if iter % 10 == 0 and iter > 0:
            log_estimated_time(f"iteration {iter}", history.iloc[0]["time"],iter, max_iter)

        past_evaluations = len(history)
        if iter % ((past_evaluations + 1) ** 2) != 0:
            return

        c.free_memory()

        log_time(f"Evaluating progress at Iteration {iter}")
        summary = pd.Series(
            {**c.state_summary(np.array([it.series for it in testing_examples]),
                               np.array([it.label for it in testing_examples]),
                               np.array([it.series for it in training_examples]),
                               np.array([it.label for it in training_examples])),
             "iter": iter, "time": perf_counter()})
        history = history.append(summary, ignore_index=True)
        if len(history) > 1:
            update_score_plot(max_iter, history)
        update_shapelet_plot(summary["S"], summary["W"])
        update_initial_shapelet_plot(history.iloc[0]["S"], history.iloc[0]["W"])
        update_confusion_matrix(predictions=summary["test_predictions"],
                                labels=[it.label for it in testing_examples])
        update_train_confusion_matrix(predictions=summary["train_predictions"],
                                labels=[it.label for it in training_examples])

        c.load_to_memory()

    def pre_init(c):
        #update_shapelet_plot(c.S, c.W)
        log_time("Init finished")


    log_time("Starting initialization")
    lts = LTSClassifier(**default_hyperparams, iter_callback=evaluate_history, post_init_callback=pre_init)
    lts.fit(np.array([it.series for it in training_examples]),
            np.array([it.label for it in training_examples]))

    # search = GridSearchCV(
    #     lts, hyperparams, cv=5, error_score="raise", iid=False
    # )
    # search.fit(
    #     [it.series for it in training_examples], [it.label for it in training_examples]
    # )
    # results = search.cv_results_


    # log_time("predicting test data")
    # predictions, probabilities = lts.predict([it.series for it in testing_examples])
    # plot_confusion_matrix(predictions, [it.label for it in testing_examples], normalize=True)

    log_time("Saving important variables")
    with open_shelve(f"./results/result.out") as sh:
        sh["classifier"] = lts
        sh["training_windows"] = training_windows
        sh["testing_windows"] = training_windows
        sh["hyperparams"] = default_hyperparams
        sh["history"] = history

    log_time("Done")
    plt.show()
