from setuptools import setup

setup(name='rollator-situation-classifier',
      install_requires=[
          "pandas",
          "numpy",
          "toolz",
          "scipy",
          "scikit-learn",
          "PyWavelets",
          "matplotlib",
          "pytest"
      ],
      zip_safe=False)
