import matplotlib.pyplot as plt
import pandas as pd
from toolz import mapcat

from main import window_size, window_spacing, sources, columns_for_classification, max_sample_interval
from plotting import show_plot
from preprocessing import split_by_time, generate_windows, apply_window
from read import read_source

source = sources[0]
samples = read_source(source)
# standardization_funs = generate_standardization_funcs_for_df(samples)
splitted_samples = split_by_time(max_sample_interval, samples)
windows = mapcat(generate_windows(window_size, window_spacing), splitted_samples)
windows = map(lambda it: it.apply(apply_window), windows)

def browse_windows():
    for i, window in enumerate(windows):
        # standardized_window = apply_per_column(standardization_funs, window)
        show_plot(window, columns_for_classification)
        print(i)
        plt.show()


def write_to_disk(df: pd.DataFrame):
    window.index.name = 'x'
    windows[columns_for_classification].to_csv("output/" + source.label + ".csv", sep=",")


browse_windows()
window = list(windows)[79]
show_plot(window, columns_for_classification)
plt.show()
write_to_disk(window)




# Walking (0)   79
# Standing      102
# Sitz nr1      17, 33
# Idle (3)      0
# Sitting       22
# Step
