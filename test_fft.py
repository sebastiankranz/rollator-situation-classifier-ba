from pandas.tests.extension.numpy_.test_numpy_nested import np
from scipy.fftpack import fft
from toolz import pipe

import plotting as pl

domain_max = 25
nsamples = 4096
sample_density = nsamples / domain_max
x = np.linspace(0, domain_max, num=nsamples)
signal = np.sin(2 * x) + np.sin(10 * x) + np.sin(20 * x)

with pl.make_figure() as ax:
    ax.plot(signal)

freqs = pipe(np.fft.fft(signal),
             np.abs,
             np.fft.fftshift
             )[2048:2248]

with pl.make_figure() as ax:
    ax.plot(freqs)

signal_coordinates = np.array([[x / sample_density, y] for x, y in enumerate(signal)])
freqs_coordinates = np.array([[x / sample_density, y] for x, y in enumerate(freqs)])

np.savetxt("fft-freqs.csv", freqs_coordinates, header="x, y", delimiter=",", comments="")
np.savetxt("fft-signal.csv", signal_coordinates, header="x, y", delimiter=",", comments="")
