import time
from termcolor import colored
from contextlib import contextmanager
from typing import (
    Any,
    Callable,
    List,
    Tuple,
    Iterable,
    Dict,
    Hashable,
    NamedTuple,
)

import numpy as np
import pandas as pd
from dataclasses import dataclass
from sklearn.cluster import KMeans
from toolz import curry

Label = Hashable
MultivariateTimeSeries = np.ndarray  # [Subseries][t]

LabeledMultivariateTimeSeries = NamedTuple(
    "LabeledMultivariateTimeSeries",
    [("series", MultivariateTimeSeries), ("label", str)],
)


@curry
def assign(columns: Iterable[pd.Series], frame: pd.DataFrame):
    """Assign multiple columns to a dataframe. 
    Columns are replaced based on the name of the Series"""

    df = frame.copy()
    for column in columns:
        df[column.name] = column

    return df


def apply_per_column(
        func_for_col: Dict[str, Callable[[pd.Series], pd.Series]], frame: pd.DataFrame
):
    df = frame.copy()
    for column, func in func_for_col.items():
        df[column] = func(df[column])
    return df


def log_time(label: str):
    print(f"{label} after: {time.perf_counter()} s")


@contextmanager
def log_duration(label: str):
    log_time(f"Starting {label}")
    yield
    log_time(f"Done {label}")


def log_estimated_time(label: str, start_time, n_passed, n_all):
    elapsed_time = elapsed_time = time.perf_counter() - start_time
    time_per_iteration = elapsed_time / (n_passed + 1)
    remaining_time = time_per_iteration * (n_all - n_passed)
    print(f"{label} {n_passed}/{n_all} after {s_to_m(elapsed_time)}m ; "
          f"time remaining: {s_to_m(remaining_time)}m")


def s_to_m(s):
    return int(s / 60)


@curry
def enumerate_to_dict(label: str, l: List):
    """
    [A, B, C, D] -> {label0: A, label1: B, label2: C, label3: D}
    """
    return {label + str(i): item for i, item in enumerate(l)}


@curry
def concat_dict(*args) -> Dict:
    new = {}
    for dict in args:
        new.update(dict)
    return new


@curry
def split_list(n_splits: int, list: List) -> List[List]:
    length = len(list)
    if length < n_splits:
        raise ValueError("n is bigger than the length of the array")
    base_split_size = length / n_splits
    remainder = length % n_splits
    indices = [
        (i * base_split_size) + min(i, remainder) for i in range(0, n_splits + 1)
    ]
    return [list[i: i + 1] for i in range(0, n_splits)]


@curry
def contains(check: Callable, l):
    return next((it for it in l if check(it)), None) is not None


def save_csv(filename, array, headers):
    np.savetxt("output/" + filename, array, delimiter=",", header=headers, comments="")


def merge_tuple(a, b) -> Tuple:
    first = a if isinstance(a, tuple) else [a]
    second = b if isinstance(b, tuple) else [b]
    return (*first, *second)


def generate_centroids(X, L_r, K):
    I, Q, M = X.shape
    J_r = Q - L_r

    def get_segments(window: MultivariateTimeSeries):
        return [window[j:(j + L_r), :].reshape(M * L_r) for j in range(J_r)]

    segments = np.array([segment for it in X for segment in get_segments(it)])

    clusterer = KMeans(n_clusters=K)
    clusterer.fit(segments)

    return np.array([it.reshape((L_r, M)) for it in clusterer.cluster_centers_])


def pick_from_probability(p):
    class_nr = np.argmax(p)
    return [1 if i == class_nr else 0 for i in range(0, len(p))]



def print_warning(string):
    print(colored(string, "red"))